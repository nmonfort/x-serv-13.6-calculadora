#!/usr/bin/python3

import sys
import decimal
import operator

#Control argumentos
if len(sys.argv) != 4:
    sys.exit("Forma de ejecución: python3 calculadora.py función operando1 operando2\n")

#Control entrada
try:
    oper1 = decimal.Decimal(sys.argv[2])
    oper2 = decimal.Decimal(sys.argv[3])
except decimal.InvalidOperation:
    sys.exit("Los operandos deben ser números")
    
#Diccionario de operaciones
ops = {
    "suma": operator.add,
    "resta": operator.sub,
    "multiplicacion": operator.mul,
    "division": operator.truediv
}

#Control de operaciones
operation = sys.argv[1]
if operation not in ["suma", "resta", "multiplicacion", "division"]:
    sys.exit("Operación inválida")

#Main
try:
    result = ops[operation](oper1, oper2)
    print(result)
except decimal.DivisionByZero:
    sys.exit("No se puede divir entre 0")
    
